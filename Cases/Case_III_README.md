# Case III

## [Transforming berberine into its intestine-absorbable form by the gut microbiota](https://pubmed.ncbi.nlm.nih.gov/26174047/)

To follow the instructions from the ChEMBL team, check this [link](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission): 

### [REFERENCE.tsv](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#reference.tsv)
This table is for the information on publication, fill out the compulsory headings as mentioned in the link above. Fill out optional if information available. 
`RIDX` heading is assigned by the depositor; e.g: it could be "Berberine_NAT_GutBac".


| RIDX | DOI | JOURNAL_NAME | YEAR | VOLUME | ISSUE | FIRST_PAGE | LAST_PAGE | REF_TYPE | TITLE | AUTHORS | ABSTRACT | PATENT_ID |
|------|-----|--------------|------|--------|-------|------------|-----------|----------|-------|---------|----------|-----------|
| Berberine_NAT_GutBac | 10.1038/srep12155 | Scientific Report | 2015 | 5 | 12155 | | | Publication | Transforming berberine into its intestine-absorbable form by the gut microbiota | ... | ... | |


### [COMPOUND_RECORD.tsv](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#compound_record.tsv)
In this study you can find all the chemicals used in this study in the section: "Methods: Chemicals and reagents" 

* Berberine (BBR)

`CIDX`, like `RIDX`, is assigned by the depositor as an ID to the compound/chemical/drug. So you can assign e.g. BGB0001. 
`COMPOUND_NAME` and `COMPOUND_KEY` can simply be the names of compounds used in the study, e.g: BBR.


### [COMPOUND_CTAB.sdf](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#compound_ctab.sdf)
Either you can create this file manually, or automatically. For automatic generation of this file, you need a column `SMILES` in your file `COMPOUND_RECORD.tsv`, which you should remove before submitting the files to ChEMBL. 
SMILES were retrieved from PubCem.

| CIDX | RIDX | COMPOUND_KEY | COMPOUND_NAME | SMILES |
|------|------|--------------|---------------|--------|
|BGB0001|Berberine_NAT_GutBac|BBR|Berberine|COC1=C(C2=C[N+]3=C(C=C2C=C1)C4=CC5=C(C=C4CC3)OCO5)OC|


*Note*
Remove SMILES column before submitting COMPOUND_RECORD.tsv to ChEMBL. The SMILES column is only added so that the strcuture and CIDX can be added together in the SDFile, but ChEMBL COMPOUND_RECORD.tsv should not contain SMILES column. 


### [ASSAY.tsv](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#assay.tsv)

For microbiome drug biotransformation, there could be different kinds of assays. In this particular study, they deal with the following assay types:

* Compound Biotransformation Assay - to measure the biotransformation of a compound and see if the expected metabolites were produced as a result of the biotransformation.

* Enzyme assay - to assess the biotransformation of a compound carried out by an enzyme and see if the expected metabolites were produced as a result of the biotransformation.

* Community assay - to measure the biotransformation of a compound in a microbiome community and see if any metabolites were produced as a result of the biotransformation.

Specifically, several of them have been conducted both in vitro and in vivo.
To differentiate assays into different categories can be complicated and needs to be carefully understood from the paper. The assay description for this paper is present in its section Methods.

Following are the mandatory headings in ASSAY.tsv file and example regarding this paper.

* AIDX: this is like CIDX, and RIDX, depositor assigned. E.g: species_strain_compound OR Escherichia coli_K-12MG1655_5-ASA. This gives basic information about the assay.
* AIDX_DESCRIPTION: this section can be flexible and give more detail on the assay
* ASSAY_TYPE: we choose A type here for ADMET assay type in case of biotransformations


An incomplete set of entry for the ASSAY.tsv for this paper could be:

#### First category: Compound Biotransformation Assay

In the current study biotransformation assays for 14 strains are reported, always exposed to BBR. Here a few examples of how I would compile the ASSAY.tsv table for such entries

| AIDX | RIDX | ASSAY_DESCRIPTION | ASSAY_TYPE | ASSAY_ORGANISM | ASSAY_STRAIN | ASSAY_TAX_ID | ASSAY_SOURCE | TARGET_TYPE | TARGET_NAME | TARGET_ACCESSION | TARGET_ORGANISM | TARGET_TAX_ID |
|------|------|-------------------|------------|---------------|-------------|---------------|---------------|-------------|-------------|------------------|---------------|--------------|
|Berberine_08-43_Staphylococcus aureus_biotransformation|Berberine_NAT_GutBac|The drug Berberine is tested on Staphylococcus aureus strain: 08-43 (collected from gastrointestinal specimens) for biotransformation. The drug concentrations were measured at 0, 12, 24, 48 and 72h. BBR and dhBBR in the culture were analyzed quantitatively using GC-MS and LC-MS/MS.|A|Staphylococcus aureus|08-43|1280|Feng|ADMET|NA|NA||NA|NA|
|Berberine_ 13-01_Enterococcus faecium_biotransformation|Berberine_NAT_GutBac|The drug Berberine is tested on Enterococcus faecium strain: 13-01 (collected from gastrointestinal specimens) for biotransformation. The drug concentrations were measured at 0, 12, 24, 48 and 72h. BBR and dhBBR in the culture were analyzed quantitatively using GC-MS and LC-MS/MS.|A|Enterococcus faecium|13-01|1352|Feng|ADMET|NA|NA|NA|NA|
|...|...|...|...|...|...|...|...|...|...|...|...|...|

#### Second category: Enzyme assay

In this study also Enzyme assays were performed to confirm that bacterial Nitroreductases are responsible for the BBR -> dhBBR biotransformation. This i how  would compile the ASSAY.tsv table to report this case:

|AIDX|RIDX|ASSAY_DESCRIPTION|ASSAY_TYPE|ASSAY_ORGANISM|ASSAY_STRAIN|ASSAY_TAX_ID|ASSAY_SOURCE|TARGET_TYPE|TARGET_NAME|TARGET_ACCESSION|TARGET_ORGANISM|TARGET_TAX_ID|
|----|----|-----------------|----------|--------------|------------|------------|------------|------------|----------|----------------|---------------|-------------|
|Berberine_Escherichia coli_biotransformation_Nitroreductase|Berberine_NAT_GutBac|The drug was incubated with nitroreductase (E. coli derived) in intestinal bacteria cultures for 0, 2, 4, 6 and 12h at 37 °C and tested for biotransformation|A|Escherichia coli|NA|562|Feng|SINGLE PROTEIN|Nitroreductase|P38489|Escherichia coli|562|


#### Third category: Community assay


| AIDX | RIDX | ASSAY_DESCRIPTION | ASSAY_TYPE | ASSAY_ORGANISM | ASSAY_STRAIN | ASSAY_TAX_ID | ASSAY_SOURCE | TARGET_TYPE | TARGET_NAME | TARGET_ACCESSION | TARGET_ORGANISM | TARGET_TAX_ID |
|------|------|-------------------|------------|----------------|--------------|--------------|--------------|-------------|-------------|------------------|----------------|------------|
|Berberine_Rat_Community_invitro_biotransformation|Berberine_NAT_GutBac|The drug is tested on 5 g extracted from the pool of 6 rats colon content for biotransformation. The drug concentrations were measured after 72h of incubation by liquid-chromatography-coupled mass spectrometry (LC-MS) and gas-chromatography-coupled mass spectrometry (GC-MS)|A|rat gut metagenome|NA|1436733|Feng|ADMET|NA|NA|NA|NA|
|Berberine_Rat_Community_invivo_biotransformation|Berberine_NAT_GutBac|Six SD rats were orally treated with BBR (200mg/kg), and their feces were collected 0, 6, 12, 24, 36, 48, and 72h after treatment. The level of dhBBR in feces was measured with GC-MS and calculated based on the standard curve obtained with the in vitro intestinal bacteria incubation mixture.|A|rat gut metagenome|NA|1436733|Feng|ADMET|NA|NA|NA|NA|


### [ASSAY_PARAM.tsv](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#assay_param.tsv)

This tsv file describes assay parameters for different assays. e.g: if AIDX Escherichia coli_K-12MG1655_5-ASA is our assay of concern, then write all possible 
parameters for this assay, and repeat for all assays.

| AIDX                               |    TYPE     | RELATION | VALUE   | UNITS | TEXT_VALUE | COMMENTS |
| ---------------------------------- | ----------------- | ---------- | ----------------- | ------------ | ------------ | ------------ |
| Escherichia coli_K-12MG1655_5-ASA  | CENTRIFUGATION    |        |  |   | Bacterial cells were harvested by centrifugation (2,500 􀀂 g,20 min, 4°C) and resuspended in 2 ml of Tris (pH 7.4)-EDTA-dithiothreitol-KCl buffer        |        | 

and so on. This should be repeated for all different assays. Each column is described in detail on the ChEMBL submission portal for further information.

### [ACTIVITY.tsv](https://chembl.gitbook.io/chembl-data-deposition-guide/untitled-10/field-names-and-data-types-basic-submission#activity.tsv)
